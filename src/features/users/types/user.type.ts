export type Role = "OWNER" | "ADMIN" | "MEMBER";

export type TUser = {
  id: string;
  name: string;
  title: string;
  email: string;
  role: Role;
};

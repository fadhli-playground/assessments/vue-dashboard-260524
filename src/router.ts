import { createWebHistory, createRouter } from "vue-router";

import ProjectsPage from "./pages/ProjectsPage.vue";
import TasksPage from "./pages/TasksPage.vue";
import IssuesPage from "./pages/IssuesPage.vue";
import UsersPage from "./pages/UsersPage.vue";

const routes = [
  { path: "/", component: UsersPage },
  { path: "/projects", component: ProjectsPage },
  { path: "/tasks", component: TasksPage },
  { path: "/issues", component: IssuesPage },
  { path: "/users", component: UsersPage },
];

export const router = createRouter({
  history: createWebHistory(),
  routes,
});

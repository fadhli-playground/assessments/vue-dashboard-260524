import { defineStore } from "pinia";
import { ref } from "vue";

export const useSidebar = defineStore("sidebar", () => {
  const isOpen = ref(true);

  function open() {
    isOpen.value = true;
  }

  function close() {
    isOpen.value = false;
  }

  return { isOpen, open, close };
});

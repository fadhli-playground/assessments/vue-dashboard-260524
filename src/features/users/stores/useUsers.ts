import { defineStore } from "pinia";
import { reactive } from "vue";
import type { TUser } from "../types/user.type";

export const useUsers = defineStore("users", () => {
  const users = reactive<Array<TUser>>([
    {
      id: "1",
      name: "Lindsay Walton",
      title: "Front-end Developer",
      email: "lindsay.walton@example.com",
      role: "MEMBER",
    },
    {
      id: "2",
      name: "Courtney Henry",
      title: "Designer",
      email: "courtney.henry@example.com",
      role: "ADMIN",
    },
    {
      id: "3",
      name: "Tom Cook",
      title: "Director of Product",
      email: "tom.cook@example.com",
      role: "MEMBER",
    },
    {
      id: "4",
      name: "Whitney Francis",
      title: "Copywriter",
      email: "whitney.francis@example.com",
      role: "MEMBER",
    },
    {
      id: "5",
      name: "Leonard Krasner",
      title: "Senior Designer",
      email: "leonard.krasner@example.com",
      role: "MEMBER",
    },
    {
      id: "6",
      name: "Floyd Miles",
      title: "Principal Designer",
      email: "floyd.miles@example.com",
      role: "MEMBER",
    },
  ]);

  function addUser(user) {
    users.push(user);
  }

  function editUser(id: TUser["id"], user: Partial<TUser>) {
    const userIndex = users.findIndex((user) => user.id === id);
    if (userIndex === -1) {
      throw new Error("User not found!");
    }

    users[userIndex] = { ...users[userIndex], ...user };
  }

  function deleteUser(id: TUser["id"]) {
    const userIndex = users.findIndex((user) => user.id === id);
    if (userIndex === -1) {
      throw new Error("User not found!");
    }

    users.splice(userIndex, 1);
  }

  return { users, addUser, editUser, deleteUser };
});
